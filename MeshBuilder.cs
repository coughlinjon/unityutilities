﻿using UnityEngine;
using System.Collections;
using JonExtends;

public class MeshBuilder : MonoBehaviour {

	private PolygonCollider2D myPolyCollider;
	private MeshFilter myMeshFilter;
	private MeshRenderer myMeshRenderer;
	private Mesh myMesh;

	// Use this for initialization
	void Start () {
		SetMyComponents ();
		BuildMesh ();
		myMeshRenderer.sortingLayerName = "Shoreline";
		myMeshRenderer.sortingOrder = 1;
	}

	private void SetMyComponents() {
		myPolyCollider = GetComponent<PolygonCollider2D> ();
		myMeshRenderer = GetComponent<MeshRenderer> ();
		myMeshFilter = GetComponent<MeshFilter> ();
	}

	// Update is called once per frame
	void Update () {
	
	}

	private void BuildMesh(){
		//gather collider points
		Vector2[] colliderPoints = myPolyCollider.points;
		//set mesh points
		Vector3[] meshPoints = new Vector3[colliderPoints.Length].To3D(colliderPoints);
		//build mesh triangles? wtf?
		Triangulator tempTriangulator = new Triangulator (colliderPoints);
		int[] triangles = tempTriangulator.Triangulate ();
		//uvs
		Vector2[] uvs = new Vector2[meshPoints.Length];
		for (int i=0; i < uvs.Length; i++) {
			uvs[i] = new Vector2(meshPoints[i].x, meshPoints[i].y);
		}

		//Make Mesh
		myMesh = new Mesh ();
		myMesh.vertices = meshPoints;
		myMesh.triangles = triangles;
		myMesh.uv = uvs;
		myMesh.RecalculateNormals();
		myMesh.RecalculateBounds();

		//Attach mesh to filter
		myMeshFilter.mesh = myMesh;

	}

}
