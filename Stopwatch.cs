﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Stopwatch : MonoBehaviour {

	public bool running;

	private float startTime = 0f;
	private float currentLapTime = 0f;
	private List<float> laps = new List<float> ();

	//
	public Text[] myDisplay;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (running) {
			currentLapTime = Time.time - startTime;
			foreach (Text watchDisplay in myDisplay)
			{
				watchDisplay.text = (Mathf.Round(currentLapTime * 100f)/100f).ToString();
			}
		}
	}

	public void ResetWatch()
	{
		laps = new List<float> ();
		StartWatch ();
	}

	public void StartWatch()
	{
		startTime = Time.time;
		currentLapTime = 0f;
		running = true;
	}

	public void LapComplete()
	{
		laps.Add (currentLapTime);
	}

	public void PauseWatch()
	{
		running = false;
	}

	public void ResumeWatch()
	{
		startTime = Time.time - currentLapTime;
		running = true;
	}

	public float LastLapTime()
	{
		if (laps.Count > 0) {
			return laps [laps.Count - 1];
		} else {
			return 0f;
		}
	}

}
