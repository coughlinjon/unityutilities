﻿using UnityEngine;
using System.Collections;
using SimpleMachines;

public class EffectsCamera : MonoBehaviour {

	//My camera
	private Camera myCamera;

	//Character to follow
	public GameObject target;

	// Use this for initialization
	void Start () {
		//Find Components
		SetComponents ();

		//shake oscillators
		shakeXOscillator = new Oscillator (shakeXTimeConstant, shakeMagX, shakeTotalPeriods);
		shakeYOscillator = new Oscillator (shakeYTimeConstant, shakeMagY, shakeTotalPeriods);
		//zoom parameters
		zoomOscillator = new Oscillator (zoomTimeConstant, zoomMag, zoomTotalPeriods);
		zoomBaseScale = myCamera.orthographicSize;
		//drift oscillators

	}

	private void SetComponents()
	{
		myCamera = this.GetComponent<Camera> ();
	}

	// Update is called once per frame
	void Update () {

		//Run all camera effects
		ApplyLagEffect();	
		ApplyShake(Time.deltaTime);
		ApplyZoomBounce (Time.deltaTime);
		ApplyFlipZoomLevel();
		ApplyDrift(Time.deltaTime);
		ApplyBurnsPan ();


		//Sum Effects into a position delta relative to target, add delta to target position
		Vector3 tempPosition = transform.position;
		tempPosition.x = target.transform.position.x + shakeDeltaPosition.x + lagDeltaPosition.x + driftDeltaPosition.x + burnsDeltaPosition.x;
		tempPosition.y = target.transform.position.y + shakeDeltaPosition.y + lagDeltaPosition.y + driftDeltaPosition.y + burnsDeltaPosition.y;

		//Update camera position
		transform.position = tempPosition;
	}

	public void AllEffectsOff()
	{
		driftOscillationEffect = false;
		permZoomEffect = false;
		zoomBounceEffect = false;
		shakeEffect = false;
		lagEffect = false;
		burnsPanEffect = false;
		ResetDeltas ();
	}

	public void ResetDeltas ()
	{
		shakeDeltaPosition = new Vector2 (0f, 0f);
		lagDeltaPosition = new Vector2 (0f, 0f);
		driftDeltaPosition = new Vector2 (0f, 0f);
		burnsDeltaPosition = new Vector2 (0f, 0f);
	}

	public void SetOrthoSize(float inputOrthoSize)
	{
		zoomBaseScale = inputOrthoSize;
		myCamera.orthographicSize = zoomBaseScale;

	}

	#region BURNS PAN

	//BurnsPan
	private bool burnsPanEffect;
	private Vector2 burnsDeltaPosition = new Vector2 (0f, 0f);
	private float burnsPanDist;
	private float burnsPanAngle;
	private float burnsZoomDist;
	private float burnsPanTimeConst;
	private float burnsPanStartTime;

	public void AddBurnsPan(float inPanDist, float inPanAngle, float inZoomDist, float inPanTimeConst)
	{
		burnsPanEffect = true;
		burnsPanDist = inPanDist;
		burnsPanAngle = inPanAngle * Mathf.PI / 180f; //convert to radians
		burnsZoomDist = inZoomDist;
		burnsPanTimeConst = inPanTimeConst;
		burnsPanStartTime = Time.time;
	}

	private void ApplyBurnsPan()
	{
		if (burnsPanEffect) 
		{
			//Calculate delta time
			float deltaTime = (Time.time - burnsPanStartTime)/burnsPanTimeConst;
			//calculate current pos/zoom offsets
			float tempPanDist = Mathf.Lerp (0, burnsPanDist, deltaTime);
			float tempZoomDist = Mathf.Lerp (0, burnsZoomDist, deltaTime);
			//Apply position offset
			Vector2 tempBurnsPanDelta = new Vector2 (Mathf.Cos (burnsPanAngle), Mathf.Sin (burnsPanAngle)) * tempPanDist;
			burnsDeltaPosition = tempBurnsPanDelta;
			//Apply Zoom 
			myCamera.orthographicSize = zoomBaseScale - tempZoomDist;//Positive zoom shrinks ortho size, Negative zoom increases ortho size
		}
	}

	#endregion

	#region LAG EFFECT

	//Lag Parameters
	public bool lagEffect;
	private Vector2 currentTargetPosition;
	private Vector2 lagDeltaPosition = new Vector2(0f,0f);
	public float maxLagDistance;

	private void ApplyLagEffect()
	{
		if (lagEffect) {
			currentTargetPosition = target.rigidbody2D.position;
			Vector2 cameraPos = new Vector2 (this.transform.position.x, this.transform.position.y);
			Vector2 cameraToTarget = currentTargetPosition - cameraPos;
			if (cameraToTarget.magnitude >= maxLagDistance) {
				lagDeltaPosition = -(cameraToTarget.normalized * maxLagDistance);
			} else {
				lagDeltaPosition = -cameraToTarget;
			}
		}
	}

	#endregion

	#region SHAKE EFFECT

	//Shake Parameters
	public bool shakeEffect;
	private PulseInput shakeInput = new PulseInput (KeyCode.F);
	private Oscillator shakeXOscillator;
	private Oscillator shakeYOscillator;
	private Vector2 shakeDeltaPosition = new Vector2(0f,0f);
	private float shakeXTimeConstant = 0.1f;
	private float shakeYTimeConstant = 0.15f;
	private float shakeTotalPeriods = 2f;
	public float shakeMagX = 0.8f;
	public float shakeMagY = 0.8f;

	private void ApplyShake(float deltaTime)
	{
		if (shakeEffect) 
		{ 
			if (shakeInput.CheckNewPulse ()) 
			{
				AddShake (shakeMagX, shakeMagY);
			}
			shakeDeltaPosition = new Vector2 (shakeXOscillator.IntegrateOscillator (deltaTime), shakeYOscillator.IntegrateOscillator (deltaTime));
		}
	}

	public void AddShake(float xMag, float yMag)
	{
		//Set shake magnitudes
		shakeXOscillator.Reset (xMag);
		shakeYOscillator.Reset (yMag);
	}

	#endregion

	#region ZOOM BOUNCE

	//ZoomBounce
	public bool zoomBounceEffect;
	private PulseInput zoomBounceInput = new PulseInput (KeyCode.G);
	private Oscillator zoomOscillator;
	private float zoomTimeConstant = 0.5f;
	private float zoomTotalPeriods = 1f;
	private float zoomMag = 0.5f;
	private float zoomBaseScale;

	private void ApplyZoomBounce(float deltaTime)
	{
		if (zoomBounceEffect) 
		{
			if (zoomBounceInput.CheckNewPulse ()) 
			{
				AddZoomBounce ();
			}
			this.GetComponent<Camera>().orthographicSize = zoomBaseScale + zoomBaseScale * zoomOscillator.IntegrateOscillator(deltaTime);
		}
	}

	public void AddZoomBounce()
	{
		zoomOscillator.Reset ();
	}

	#endregion
	
	#region FLIP ZOOM

	//PermZoom
	public bool permZoomEffect;
	private PulseInput permZoomInput = new PulseInput(KeyCode.R);
	public float minZoomScale;
	public float maxZoomScale;

	private void ApplyFlipZoomLevel()
	{
		if (permZoomEffect) 
		{
			if (permZoomInput.CheckNewPulse())
			{
				if (zoomBaseScale == minZoomScale) 
				{
					zoomBaseScale = maxZoomScale;
					this.GetComponent<Camera> ().orthographicSize = zoomBaseScale;
				} 
				else 
				{
					zoomBaseScale = minZoomScale;
					this.GetComponent<Camera> ().orthographicSize = zoomBaseScale;
				}
			}
		}
	}

	#endregion

	#region DRIFT OSCILLATOR

	//PeriodicDrift
	public bool driftOscillationEffect;
	private Oscillator driftXOscillator;
	public float driftXOscillatorTimeConst = 3f;
	public float driftXOscillatorAmplitude = 2.5f;
	private float driftXOscillatorPeriods = 1000000f;
	private Oscillator driftYOscillator;
	public float driftYOscillatorTimeConst = 3f;
	public float driftYOscillatorAmplitude = 2.5f;
	private float driftYOscillatorPeriods = 1000000f;
	private Vector2 driftDeltaPosition = new Vector2(0f,0f);
	public float jitterFrequencyHz = 40f;
	public float jitterPhasePct = 1f;

	private void ApplyDrift(float deltaTime)
	{
		if (driftOscillationEffect) {
			driftDeltaPosition.x = driftXOscillator.IntegrateOscillator (deltaTime);
			driftDeltaPosition.y = driftYOscillator.IntegrateOscillator (deltaTime);
		}
	}

	public void ResetDriftOscillation()
	{
		driftXOscillator.Reset();
		driftYOscillator.Reset();
	}

	public void EnableDriftOscillation()
	{
		driftOscillationEffect = true;
	}

	public void DisableDriftOscillation()
	{
		driftOscillationEffect = false;
	}

	public void InitiateNewDriftOscillation()
	{
		driftXOscillator = new Oscillator (driftXOscillatorTimeConst, driftXOscillatorAmplitude, driftXOscillatorPeriods);
		driftYOscillator = new Oscillator (driftYOscillatorTimeConst, driftYOscillatorAmplitude, driftYOscillatorPeriods);
		driftXOscillator.Reset ();
		driftYOscillator.Reset ();
		driftOscillationEffect = true;
	}

	public void InitiateNewDriftOscillation(float xTimeConst, float xAmp, float yTimeConst, float yAmp)
	{
		driftXOscillatorAmplitude = xAmp;
		driftXOscillatorTimeConst = xTimeConst;
		driftYOscillatorAmplitude = yAmp;
		driftYOscillatorTimeConst = yTimeConst;
		driftXOscillator = new Oscillator (driftXOscillatorTimeConst, driftXOscillatorAmplitude, driftXOscillatorPeriods);
		driftYOscillator = new Oscillator (driftYOscillatorTimeConst, driftYOscillatorAmplitude, driftYOscillatorPeriods);
		driftXOscillator.Reset ();
		driftYOscillator.Reset ();
		driftOscillationEffect = true;
	}

	public void InitiateJitter(float jitterMag)
	{
		//Use default jitter frequency
		float xJitterTimeConst = 1f / jitterFrequencyHz;
		float yJitterTimeConst = 1f / (jitterFrequencyHz * (100f - jitterPhasePct));
		InitiateNewDriftOscillation (xJitterTimeConst, jitterMag, yJitterTimeConst, jitterMag);
	}

	#endregion
}
