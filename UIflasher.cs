﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleMachines;

public class UIflasher : MonoBehaviour {

	public bool flashing;
	public float period;
	private Oscillator myFlasher;

	//Kids
	private List<GameObject> myKids = new List<GameObject>();

	// Use this for initialization
	void Start () {
		myFlasher = new Oscillator (period, 1f, -1f);
		myFlasher.Reset ();
		GetKids ();
	}

	private void GetKids(){
		foreach (Transform kid in transform) {
			myKids.Add(kid.gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (flashing) {
			RenderKids(myFlasher.IntegrateOscillator(Time.deltaTime) > -0.5f);
		}
	}

	void RenderKids(bool showThem)
	{
		foreach (GameObject kid in myKids) {
			kid.SetActive(showThem);
		}
	}
}
