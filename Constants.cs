﻿using UnityEngine;
using System.Collections;

namespace Utilities1
{

	public static class Constants 
	{
		public static float Rad2Deg()
		{
			return 180f / Mathf.PI;
		}

		public static float Deg2Rad()
		{
			return Mathf.PI / 180f;
		}
	}
}